��          ,       <       <   �  =       9  �  :   
<h3>Software Developer Entry Level</h3>
Neox Capital is a start-up Investment Manager with big visions and as such is seeking to expand its IT-department. You will join a small team of passionate and highly skilled people at the heart of Europe in Luxembourg, developing brand new products as well as maintaining the existing ones. At Neox Capital, we offer a start-up atmosphere backed by years of experience and as such entry level position is poised for growth and personal development.
<h3>Main responsibilities:</h3>
<ul>
 	<li>Design, development and testing of new features and applications</li>
 	<li>Be responsible for regular communication with colleagues involved in the development process</li>
 	<li>Implement, test and bug-fix functionality</li>
 	<li>Be responsibility for design and implementation of software projects using languages such as VBA, R, Python, SQL</li>
 	<li>Design, build, and maintain efficient and reliable code</li>
</ul>
<h3>Required competencies:</h3>
<ul>
 	<li>You have at least a degree in Computer Science or equivalent work experience.</li>
 	<li>You have some professional experience, or recently graduated.</li>
 	<li>Knowledge of VBA/SQL</li>
 	<li>You are fluent in English (mandatory) and preferable in a Scandinavian language.</li>
</ul>
<h3>In addition to the requirements listed above, we highly value candidates that:</h3>
<ul>
 	<li>Have an interest in financial markets</li>
 	<li>Possess general IT knowledge</li>
</ul>
We evaluate applications on a continuous basis, and schedule interviews accordingly. To apply, please submit a CV and a short motivational letter. Please direct questions and applications Hampus Hagström:

<a href="#">Hampus.hagstrom@neoxcapital.com</a>
tel: +352 6710 14033

  
<h3>Software Developer Entry Level</h3>
Neox Capital is a start-up Investment Manager with big visions and as such is seeking to expand its IT-department. You will join a small team of passionate and highly skilled people at the heart of Europe in Luxembourg, developing brand new products as well as maintaining the existing ones. At Neox Capital, we offer a start-up atmosphere backed by years of experience and as such entry level position is poised for growth and personal development.
<h3>Main responsibilities:</h3>
<ul>
 	<li>Design, development and testing of new features and applications</li>
 	<li>Be responsible for regular communication with colleagues involved in the development process</li>
 	<li>Implement, test and bug-fix functionality</li>
 	<li>Be responsibility for design and implementation of software projects using languages such as VBA, R, Python, SQL</li>
 	<li>Design, build, and maintain efficient and reliable code</li>
</ul>
<h3>Required competencies:</h3>
<ul>
 	<li>You have at least a degree in Computer Science or equivalent work experience.</li>
 	<li>You have some professional experience, or recently graduated.</li>
 	<li>Knowledge of VBA/SQL</li>
 	<li>You are fluent in English (mandatory) and preferable in a Scandinavian language.</li>
</ul>
<h3>In addition to the requirements listed above, we highly value candidates that:</h3>
<ul>
 	<li>Have an interest in financial markets</li>
 	<li>Possess general IT knowledge</li>
</ul>
We evaluate applications on a continuous basis, and schedule interviews accordingly. To apply, please submit a CV and a short motivational letter. Please direct questions and applications Hampus Hagström:

<a href="#">Hampus.hagstrom@neoxcapital.com</a>
tel: +352 6710 14033

 