��          ,       <       <   |  =       �  U  �   <h3>Data Scientist Entry Level</h3>

Vill du arbeta på ett litet företag med stora visioner?

Neox Capital erbjuder möjligheten att bli en viktig del av ett litet team med passionerade och
kompetenta individer i hjärtat av Europa i Luxemburg. Som Data Scientist ligger huvudfokus
inom ramarna för datavetenskap så som dataleveransmekanismer, dataintegritetslösningar och
analys.

<h3>Kompetenskrav:</h3>
<ul>
<li>Du har åtminstone en examen i <strong>Datavetenskap, Matematik</strong> eller ekvivalent
arbetslivserfarenhet</li>
<li>Du har arbetsliveserfarenhet eller är nyexaminerad.</li>
<li>Kan SQL/mySQL</li>
<li>Talar <strong>flytande engelska</strong>, samt helst även ett skandinaviskt språk.</li>
</ul>

<strong>Utöver dessa krav</strong> så värderar vi kandidater högt som:

<ul>
<li>Har ett intresse av finansmarknader</li>
<li>Har generella IT-kunskaper</li>
<li>Kan ytterligare ett programmeringsspråk (VBA/R/Python eller liknande)</li>
</ul>

<h3>Om oss</h3>

Neox Capital kombinerar banbrytande matematisk och statistisk analys för att förbättra
träffsäkerheten i investeringsbesluten sen tio år tillbaka. Neox Capital erbjuder en startup-
atmosfär byggd på många års erfarenhet inom fältet. Således erbjuder anställningen stor
möjlighet för både professionell som personlig utveckling.

Vi utvärderar alla ansökningar kontinuerligt och schemalägger intervjuer därefter. För att ansöka,
vänligen skicka ett CV och ett personligt brev. Frågor och/eller ansökningar hänvisas till Hampus
Hagström:

<a href="#">Hampus.hagstrom@neoxcapital.com</a>

tel: +352 6710 14033  <h3>Data Scientist Entry Level</h3>

Vill du arbeta på ett litet företag med stora visioner?

Neox Capital erbjuder möjligheten att bli en viktig del av ett litet team med passionerade och
kompetenta individer i hjärtat av Europa i Luxemburg. Som Data Scientist ligger huvudfokus
inom ramarna för datavetenskap så som dataleveransmekanismer, dataintegritetslösningar och
analys.

<h3>Kompetenskrav:</h3>
<ul>
<li>Du har åtminstone en examen i <strong>Datavetenskap, Matematik</strong> eller ekvivalent
arbetslivserfarenhet</li>
<li>Du har arbetsliveserfarenhet eller är nyexaminerad.</li>
<li>Kan SQL/mySQL</li>
<li>Talar <strong>flytande engelska</strong>, samt helst även ett skandinaviskt språk.</li>
</ul>

<strong>Utöver dessa krav</strong> så värderar vi kandidater högt som:

<ul>
<li>Har ett intresse av finansmarknader</li>
<li>Har generella IT-kunskaper</li>
<li>Kan ytterligare ett programmeringsspråk (VBA/R/Python eller liknande)</li>
</ul>

<h3>Om oss</h3>

Neox Capital kombinerar banbrytande matematisk och statistisk analys för att förbättra
träffsäkerheten i investeringsbesluten sen tio år tillbaka. Neox Capital erbjuder en startup-
atmosfär byggd på många års erfarenhet inom fältet. Således erbjuder anställningen stor
möjlighet för både professionell som personlig utveckling.

Vi utvärderar alla ansökningar kontinuerligt och schemalägger intervjuer därefter. För att ansöka,
vänligen skicka ett CV och ett personligt brev. Frågor och/eller ansökningar hänvisas till Hampus
Hagström:

<a href="#">Hampus.hagstrom@neoxcapital.com</a>

tel: +352 6710 14033 