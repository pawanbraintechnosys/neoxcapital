��          L       |       |   v   }   u   �   x   j  	  �  @  �
      0  �   1  �   �  �   P  �  �  z  �   
<p style="text-align: center;"><a href="/?page_id=1012" target="_blank" rel="noopener noreferrer">Feedback</a></p>
 
<p style="text-align: center;"><a href="/?page_id=1012" target="_blank" rel="noopener noreferrer">Licence</a></p>
 
<p style="text-align: center;"><a href="/?page_id=763" target="_blank" rel="noopener noreferrer">Disclaimer </a></p>
 <div class="peter-image">
<h6>No opinions. Just data.</h6>
These are exciting times. Never has there been so much data about our society, economy and its companies. At the same time, our experiences are built on research and history in an unprecedented way.

In addition, today we have computing power that could not be dreamed of. In a few seconds we can now perform calculations that recently took us several weeks. This is evident in everything from biotechnology to energy research and also money management.

In other words, we have a golden opportunity to let the numbers speak for themselves and work for us. They can help people make better decisions, reduce stress and misunderstandings, and it allows us to act more long-term and really solve the big issues of our time.

Two big traps are also evident in the world of information, we must admit. Too much incorrect data has become a central problem – information overflow. We call it noise and misleading facts. We identify as much rubbish as 38 percent of all data we collect about the stock exchange's companies. It is quite a lot, and it also explains why so many market participants misinterpret data.

In addition, we must teach people to trust facts, and avoid placing our feelings in between. Scientists have been talking about this for decades - that we have to get away from all the beliefs, temporary concerns and optimism. There are, after all, pure facts that show what is real, empowering and based on the long term.

This philosophy is the foundation of Neox’s surprisingly stable and strong management year after year. Yes, we are surprised how facts can have such a positive impact. Obviously, there's a lot left to be done here.

We at Neox now want to continue. We want to show that facts can build values, as few others. We want to contribute to a fact-rich worldview that reduces misunderstanding, lays the foundation for wise decisions and builds a common path forward.
<p class="welcome-neox">Welcome to Neox.</p>

<div class="about-row">
<div class="aboutname">
<p class="founder-test">Peter Sjöholm, founder</p>
</div>
<div class="clind-imags">
<img src="https://neoxcapital.com/wp-content/uploads/2020/02/20191226_144729.jpg" alt=""  class="alignnone size-full wp-image-1955" />
</div>
</div>
</div> <div class="table-1">
	<table width="100%">
		<tbody>
			<tr class="">
				<td align="left">Date</td>
				<td align="left">Position</td>
				<td align="left">Placement</td>
			</tr>
			<tr>
				<td align="left">2020-05-04</td>
				<td align="left"><a href="/?page_id=1806">Junior Software Developer</a></td>
				<td align="left">Luxembourg</td>
			</tr>		
			<tr>
				<td align="left">2020-05-04</td>
				<td align="left"><a href="/?page_id=2556">Data Scientist Entry Level</a></td>
				<td align="left">Luxembourg</td>
			</tr>
		</tbody>
	</table>
</div>  
<p style="text-align: center;"><a href="https://neoxcapital.com/complain/?lang=sv" target="_blank" rel="noopener noreferrer">Feedback</a></p>
 
<p style="text-align: center;"><a href="https://neoxcapital.com/complain/?lang=sv" target="_blank" rel="noopener noreferrer">Licence</a></p>
 
<p style="text-align: center;"><a href="https://neoxcapital.com/disclaimer/?lang=sv" target="_blank" rel="noopener noreferrer">Disclaimer </a></p>
 <div class="peter-image">
<h6>No opinions. Just data.</h6>
These are exciting times. Never has there been so much data about our society, economy and its companies. At the same time, our experiences are built on research and history in an unprecedented way.

In addition, today we have computing power that could not be dreamed of. In a few seconds we can now perform calculations that recently took us several weeks. This is evident in everything from biotechnology to energy research and also money management.

In other words, we have a golden opportunity to let the numbers speak for themselves and work for us. They can help people make better decisions, reduce stress and misunderstandings, and it allows us to act more long-term and really solve the big issues of our time.

Two big traps are also evident in the world of information, we must admit. Too much incorrect data has become a central problem – information overflow. We call it noise and misleading facts. We identify as much rubbish as 38 percent of all data we collect about the stock exchange's companies. It is quite a lot, and it also explains why so many market participants misinterpret data.

In addition, we must teach people to trust facts, and avoid placing our feelings in between. Scientists have been talking about this for decades - that we have to get away from all the beliefs, temporary concerns and optimism. There are, after all, pure facts that show what is real, empowering and based on the long term.

This philosophy is the foundation of Neox’s surprisingly stable and strong management year after year. Yes, we are surprised how facts can have such a positive impact. Obviously, there's a lot left to be done here.

We at Neox now want to continue. We want to show that facts can build values, as few others. We want to contribute to a fact-rich worldview that reduces misunderstanding, lays the foundation for wise decisions and builds a common path forward.
<p class="welcome-neox">Welcome to Neox.</p>

<div class="about-row">
<div class="aboutname">
<p class="founder-test">Peter Sjöholm, founder</p>
</div>
<div class="clind-imags">
<img src="https://neoxcapital.com/wp-content/uploads/2020/02/20191226_144729.jpg" alt="" class="alignnone size-full wp-image-1955">
</div>
</div>
</div> <div class="table-1">
	<table width="100%">
		<tbody>
			<tr class="">
				<td align="left">Date</td>
				<td align="left">Position</td>
				<td align="left">Placement</td>
			</tr>
			<tr>
				<td align="left">2020-05-04</td>
				<td align="left"><a href="https://neoxcapital.com/software-developer-job/?lang=sv">Junior Software Developer</a></td>
				<td align="left">Luxembourg</td>
			</tr>		
			<tr>
				<td align="left">2020-05-04</td>
				<td align="left"><a href="https://neoxcapital.com/data-scientist-job/?lang=sv">Data Scientist Entry Level</a></td>
				<td align="left">Luxembourg</td>
			</tr>
		</tbody>
	</table>
</div> 