��          �       �      �  #  �  N   �  �    h  �     2     M     l     n  	   �  	   �     �     �     �  &   �  (     '   ?  $   g  A   �  ?   �  [     P   j  P   �  5         B  �  C  J   2  �  }  `  ,#     �'     �'     �'     �'  	   �'  	   �'     �'     	(     ((  &   F(  (   m(  '   �(  $   �(  A   �(  ?   %)  [   e)  P   �)  P   *  5   c*   

<section class="pt-0">
<div class="table-1">
<div class="table-1">
<div class="table-1">
<table class="table table-striped ">
<thead>
<tr>
<th>Fund</th>
<th>NAV</th>
<th>Datum</th>
<th>% 1 dag</th>
<th>% Sedan start</th>
<th>Bench % 1 dag</th>
<th>Bench % sedan start</th>
</tr>
</thead>
<tbody>
<tr>
<td>Lux Multimanager Europe Equity Select Retail SEK</td>
<td>116.28</td>
<td>2020-05-20</td>
<td style="text-align: right;">0.98%</td>
<td style="text-align: right;">16.28%</td>
<td style="text-align: right;">0.93%</td>
<td style="text-align: right;">3.86%</td>
</tr>
<tr>
<td>Lux Multimanager Europe Equity Select Retail EUR</td>
<td>91.45</td>
<td>2020-05-20</td>
<td style="text-align: right;">1.43%</td>
<td style="text-align: right;">-8.55%</td>
<td style="text-align: right;">1.32%</td>
<td style="text-align: right;">-19.14%</td>
</tr>
<tr>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>
Benchmark: STOXX Europe 600

</div>
</div>
</div>
</section> 

Neox förvaltning har utvecklats starkare än börsen oavsett riktning

 
<h3 data-fontsize="27" data-lineheight="40">Datadriven fond</h3>
Att våra portföljer har så stark historik beror på vårt sätt att analysera bolag. Vi har flyttat analytikerna in i datorn. På det sättet kan vi samla och analysera mer och bredare data än de flesta andra. Lägg till att vi är särskilt duktiga på att städa bort ovidkommande fakta. Med datorer undviker vi också mänskliga åsikter, smak och misstag.

Det gör att vi inte reagerar på rykten och korta nyheter. Våra datorer analyserar bara fakta. Sedan har vi en gedigen modell för att analysera bolag i flera steg. Kunskaperna om bolagsanalys har vi hämtat från erkända investerare som Buffet, Graham och psykologer som Kahneman. Erfarenheterna att analysera data kommer från Neox mångåriga utvecklingsarbete.

Vår datadrivna fond letar efter stora europeiska kvalitetsbolag som vi kan äga i många år. Vi behåller bolagen även när de blivit lite dyra, för våra val av bolag visar sig slå börsen under lång tid. Ofta kan vi behålla samma bolag i 5-7 år och ibland längre, vilket är mycket ovanligt i fondvärlden. Det gör att fonden kan tåla börsoro bättre och dessutom vara stark i goda tider. Fonden innehåller 25 - 30 stora europeiska bolag. Den som sparar i fonden kan därmed få styrkan i många olika bolag och är inte i händerna på enskilda bolag. Vi väljer stora kvalitetsbolag med lång historik.

Gör datorer allt rätt? Det mesta har blivit väldigt rätt. Det visar våra flera år av historik. Samtidigt kan vi alltid bli bättre. Eftersom varje steg är noga dokumenterat så kan vi fortsätta jobba och göra våra modeller lite bättre med tiden. Vår fasta fondavgift är 0,8% för retailfonderna. Det är lägre än snittet bland Europafonder. Jämför vi med fonder som säger sig vilja slå börssnittet, är vi väldigt billiga. Lägg till att vi inte bara pratar om ”att slå börsen”, utan vi har siffror som visar på att vi har klarat av det under många år. Därför har vi förutom fasta avgiften 0,8% även en andel av fondens framgång - bättre än börsen; 15 % av tillväxten över marknaden. Det blir en avgift som vi bara tar ut de år fonden växer bättre börsen.

 
<h3>En riktigt bra Europafond</h3>
Riktigt bra fonder ska ge stabil tillväxt bättre än börsen många år framöver. Med den ambitionen startar vi nu en Europafond, som lutar sig mot 9 års stark historik i våra Europa portföljer.

Bra fonder ska ge stark tillväxt i goda tider och samtidigt stå emot oron vid svag börs. Det har vi sett Neox portföljer göra under många år. När börsen är stark har våra portföljer stigit i värde snabbare än omvärlden. I tider av mer ojämn ekonomi har våra portföljer ändå stått emot bättre än de flesta andra. Fonden följer nu exakt samma analysmetod. Stark stabil fond Den analysmetoden har i genomsnitt gett annualiserad avkastning på 17% per år med en betarisk på 0.62 mellan 2015 och 2020.

Vår Europa portfölj föll som mest med 30% mot marknadens 36%. Dock är hela fallet återtagit och portföljerna är upp 10% för året. Samtidigt ska vi påpeka att vår Europafond en aktiefond. Den växer med ekonomin och backar i tillfälliga börsnedgångar. Vår historik visar samtidigt att våra portföljer tappar mindre än vanliga Europafonder.

 
<h3>Neox Insikter</h3>
 
<h3>Samarbetspartners</h3>
 # #Samarbetspartners 2692|full 2738|full Köp Fonden hos Avanza (SEK) Köp Fonden hos Nordnet i Euro Köp Fonden hos Nordnet i SEK Köp Fonden hos annan samarbetspartner Läs våra mest vanliga frågor och svar Neox Europaportfölj har slagit börsen https://neoxcapital.com/f-s/?lang=sv https://neoxcapital.com/wp-content/uploads/2020/05/rund_graph.png https://neoxcapital.com/wp-content/uploads/2020/05/side-img.png https://www.avanza.se/fonder/om-fonden.html/1096712/lux-multimanager-sicav-eur-eq-sel-b-sek https://www.nordnet.se/marknaden/fondlistor/17264065-lux-multi-manager-sicav-eur https://www.nordnet.se/marknaden/fondlistor/17264068-lux-multi-manager-sicav-eur https://www.youtube.com/watch?v=RX05xernWFs&amp;t=15s  

<section class="pt-0">
<div class="table-1">
<div class="table-1">
<div class="table-1">
<table class="table table-striped ">
<thead>
<tr>
<th>Fund</th>
<th>NAV</th>
<th>Datum</th>
<th>% 1 dag</th>
<th>% Sedan start</th>
<th>Bench % 1 dag</th>
<th>Bench % sedan start</th>
</tr>
</thead>
<tbody>
<tr>
<td>Lux Multimanager Europe Equity Select Retail SEK</td>
<td>116.28</td>
<td>2020-05-20</td>
<td style="text-align: right;">0.98%</td>
<td style="text-align: right;">16.28%</td>
<td style="text-align: right;">0.93%</td>
<td style="text-align: right;">3.86%</td>
</tr>
<tr>
<td>Lux Multimanager Europe Equity Select Retail EUR</td>
<td>91.45</td>
<td>2020-05-20</td>
<td style="text-align: right;">1.43%</td>
<td style="text-align: right;">-8.55%</td>
<td style="text-align: right;">1.32%</td>
<td style="text-align: right;">-19.14%</td>
</tr>
<tr>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>
Benchmark: STOXX Europe 600

</div>
</div>
</div>
</section> 

Neox förvaltning har utvecklats starkare än börsen oavsett riktning

 
<h3 data-fontsize="27" data-lineheight="40">Datadriven fond</h3>
Att våra portföljer har så stark historik beror på vårt sätt att analysera bolag. Vi har flyttat analytikerna in i datorn. På det sättet kan vi samla och analysera mer och bredare data än de flesta andra. Lägg till att vi är särskilt duktiga på att städa bort ovidkommande fakta. Med datorer undviker vi också mänskliga åsikter, smak och misstag.

Det gör att vi inte reagerar på rykten och korta nyheter. Våra datorer analyserar bara fakta. Sedan har vi en gedigen modell för att analysera bolag i flera steg. Kunskaperna om bolagsanalys har vi hämtat från erkända investerare som Buffet, Graham och psykologer som Kahneman. Erfarenheterna att analysera data kommer från Neox mångåriga utvecklingsarbete.

Vår datadrivna fond letar efter stora europeiska kvalitetsbolag som vi kan äga i många år. Vi behåller bolagen även när de blivit lite dyra, för våra val av bolag visar sig slå börsen under lång tid. Ofta kan vi behålla samma bolag i 5-7 år och ibland längre, vilket är mycket ovanligt i fondvärlden. Det gör att fonden kan tåla börsoro bättre och dessutom vara stark i goda tider. Fonden innehåller 25 - 30 stora europeiska bolag. Den som sparar i fonden kan därmed få styrkan i många olika bolag och är inte i händerna på enskilda bolag. Vi väljer stora kvalitetsbolag med lång historik.

Gör datorer allt rätt? Det mesta har blivit väldigt rätt. Det visar våra flera år av historik. Samtidigt kan vi alltid bli bättre. Eftersom varje steg är noga dokumenterat så kan vi fortsätta jobba och göra våra modeller lite bättre med tiden. Vår fasta fondavgift är 0,8% för retailfonderna. Det är lägre än snittet bland Europafonder. Jämför vi med fonder som säger sig vilja slå börssnittet, är vi väldigt billiga. Lägg till att vi inte bara pratar om ”att slå börsen”, utan vi har siffror som visar på att vi har klarat av det under många år. Därför har vi förutom fasta avgiften 0,8% även en andel av fondens framgång - bättre än börsen; 15 % av tillväxten över marknaden. Det blir en avgift som vi bara tar ut de år fonden växer bättre börsen.

 
<h3>En riktigt bra Europafond</h3>
Riktigt bra fonder ska ge stabil tillväxt bättre än börsen många år framöver. Med den ambitionen startar vi nu en Europafond, som lutar sig mot 9 års stark historik i våra Europa portföljer.

Bra fonder ska ge stark tillväxt i goda tider och samtidigt stå emot oron vid svag börs. Det har vi sett Neox portföljer göra under många år. När börsen är stark har våra portföljer stigit i värde snabbare än omvärlden. I tider av mer ojämn ekonomi har våra portföljer ändå stått emot bättre än de flesta andra. Fonden följer nu exakt samma analysmetod. Stark stabil fond Den analysmetoden har i genomsnitt gett annualiserad avkastning på 17% per år med en betarisk på 0.62 mellan 2015 och 2020.

Vår Europa portfölj föll som mest med 30% mot marknadens 36%. Dock är hela fallet återtagit och portföljerna är upp 10% för året. Samtidigt ska vi påpeka att vår Europafond en aktiefond. Den växer med ekonomin och backar i tillfälliga börsnedgångar. Vår historik visar samtidigt att våra portföljer tappar mindre än vanliga Europafonder.

 
<h3>Neox Insikter</h3>
 
<h3>Samarbetspartners</h3>
 # #Samarbetspartners 2692|full 2738|full Köp Fonden hos Avanza (SEK) Köp Fonden hos Nordnet i Euro Köp Fonden hos Nordnet i SEK Köp Fonden hos annan samarbetspartner Läs våra mest vanliga frågor och svar Neox Europaportfölj har slagit börsen https://neoxcapital.com/q-a/?lang=sv https://neoxcapital.com/wp-content/uploads/2020/05/rund_graph.png https://neoxcapital.com/wp-content/uploads/2020/05/side-img.png https://www.avanza.se/fonder/om-fonden.html/1096712/lux-multimanager-sicav-eur-eq-sel-b-sek https://www.nordnet.se/marknaden/fondlistor/17264065-lux-multi-manager-sicav-eur https://www.nordnet.se/marknaden/fondlistor/17264068-lux-multi-manager-sicav-eur https://www.youtube.com/watch?v=RX05xernWFs&amp;t=15s 