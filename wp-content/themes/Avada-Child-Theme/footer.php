<?php
/**
 * The footer template.
 *
 * @package Avada
 * @subpackage Templates
 */

// Do not allow directly accessing this file.
if ( ! defined( 'ABSPATH' ) ) {
	exit( 'Direct script access denied.' );
}
?>
						<?php do_action( 'avada_after_main_content' ); ?>

					</div>  <!-- fusion-row -->
				</main>  <!-- #main -->
				<?php do_action( 'avada_after_main_container' ); ?>

				<?php
				/**
				 * Get the correct page ID.
				 */
				$c_page_id = Avada()->fusion_library->get_page_id();
				?>

				<?php
				/**
				 * Only include the footer.
				 */
				?>
				<?php if ( ! is_page_template( 'blank.php' ) ) : ?>
					<?php $footer_parallax_class = ( 'footer_parallax_effect' === Avada()->settings->get( 'footer_special_effects' ) ) ? ' fusion-footer-parallax' : ''; ?>

					<div class="fusion-footer<?php echo esc_attr( $footer_parallax_class ); ?>">
						<?php get_template_part( 'templates/footer-content' ); ?>
					</div> <!-- fusion-footer -->

					<div class="fusion-sliding-bar-wrapper">
						<?php
						/**
						 * Add sliding bar.
						 */
						if ( Avada()->settings->get( 'slidingbar_widgets' ) ) {
							get_template_part( 'sliding_bar' );
						}
						?>
					</div>

					<?php do_action( 'avada_before_wrapper_container_close' ); ?>
				<?php endif; // End is not blank page check. ?>
			</div> <!-- wrapper -->
		</div> <!-- #boxed-wrapper -->
		<div class="fusion-top-frame"></div>
		<div class="fusion-bottom-frame"></div>
		<div class="fusion-boxed-shadow"></div>
		<a class="fusion-one-page-text-link fusion-page-load-link"></a>

		<div class="avada-footer-scripts">
			<?php wp_footer(); ?>
		</div>
        <script>
        var lang = jQuery("html").attr('lang');
        if('sv-SE' == lang){
        jQuery(".fusion-read-more").text('Läs mer');
		 jQuery(".pagination-next .page-text").text('Nästa');
		jQuery(".pagination-prev .page-text").text('Föregående');
		jQuery('.ur-form-grid p label[for="username"]').text('Användarnamn eller emailaddress');
		jQuery('.ur-form-grid p label[for="password"]').text('Lösenord');
		jQuery('.ur-form-grid .user-registration-form__label-for-checkbox span').text('Kom ihåg mig');
		jQuery('.user-registration-LostPassword a').text('Förlorat ditt lösenord?');
		jQuery('.user-registration-Button.button').val('logga in');
		jQuery('.user-registration-ResetPassword .ur-form-grid p:nth-child(1)').text('Förlorat ditt lösenord? Ange ditt användarnamn eller din e-postadress. Du kommer att få en länk för att skapa ett nytt lösenord via e-post.');
		jQuery('.user-registration-ResetPassword .user-registration-form-row label[for="user_login"]').text('Användarnamn eller e-mail');
        }
        </script>
		<?php 
			$currentURl =site_url();
			$currentLng = ICL_LANGUAGE_CODE;
			$fullUrl =$currentURl.'/insight/insights-overview/?lang='.$currentLng;
		?>

		<script>
			jQuery(document).ready(function(){
				var currenUrl = "<?php echo $fullUrl; ?>";
				jQuery(".home_insight ul.slides a").attr("href",currenUrl);
				jQuery(".home_insight .recent-posts-content .entry-title a").attr("href",currenUrl);
			});
			</script>

<?php if(is_page('Method') || is_page('Metod')): ?>
<script type="text/javascript">
	//Method Page JS
jQuery(document).ready(function(){	
	jQuery(window).scroll(function(){
  	var sticky = jQuery('.method-sticky-header'),
    scroll = jQuery(window).scrollTop();
    // console.log(scroll);
	//var section = jQuery('.fusion-fullwidth').length;
        var section = jQuery('.Sticky-custom-menu .menu-item-1705 ul li').length;
	var scrollHeight = jQuery(document).height();
	var persec_hieght = scrollHeight/section;
	var scrollPosition = jQuery(window).height() + jQuery(window).scrollTop();
	// console.log(jQuery('#page_id').val());
	// console.log(scrollHeight);
	// console.log(persec_hieght);
	// console.log(scrollPosition);
	/* Start For Method page */
	/*Start in swidesh*/
	if(scroll <= (600)) {
		var $sbmenu1 = jQuery('.Sticky-custom-menu .menu-item-1705 ul li:nth-child(1) a');
		jQuery('.custom-title h1').html($sbmenu1.html());

	} else if(scroll <= (600+600)) {
		var $sbmenu2 = jQuery('.Sticky-custom-menu .menu-item-1705 ul li:nth-child(2) a');
		jQuery('.custom-title h1').html($sbmenu2.html());

	} else if(scroll <= (600+600+600)) {
		var $sbmenu3 = jQuery('.Sticky-custom-menu .menu-item-1705 ul li:nth-child(3) a');	
		jQuery('.custom-title h1').html($sbmenu3.html());

	} else if(scroll <= (600+600+600+700)) {
		var $sbmenu4 = jQuery('.Sticky-custom-menu .menu-item-1705 ul li:nth-child(4) a');
		jQuery('.custom-title h1').html($sbmenu4.html());

	} else {
		var $sbmenu5 = jQuery('.Sticky-custom-menu .menu-item-1705 ul li:nth-child(5) a');
		jQuery('.custom-title h1').html($sbmenu5.html());
	}	
	/*start For english */
	if(scroll <= (600)) {
		var $sbmenu1 = jQuery('.Sticky-custom-menu .menu-item-1122 ul li:nth-child(1) a');
		jQuery('.custom-title h1').html($sbmenu1.html());

	} else if(scroll <= (600+600)) {
		var $sbmenu2 = jQuery('.Sticky-custom-menu .menu-item-1122 ul li:nth-child(2) a');
		jQuery('.custom-title h1').html($sbmenu2.html());

	} else if(scroll <= (600+600+600)) {
		var $sbmenu3 = jQuery('.Sticky-custom-menu .menu-item-1122 ul li:nth-child(3) a');	
		jQuery('.custom-title h1').html($sbmenu3.html());

	} else if(scroll <= (600+600+600+700)) {
		var $sbmenu4 = jQuery('.Sticky-custom-menu .menu-item-1122 ul li:nth-child(4) a');
		jQuery('.custom-title h1').html($sbmenu4.html());
		
	} else {
		var $sbmenu5 = jQuery('.Sticky-custom-menu .menu-item-1122 ul li:nth-child(5) a');
		jQuery('.custom-title h1').html($sbmenu5.html());
	}
	/*End For english */
	/* Start For Method page */
	 if (scroll >= 100) { 	
	 sticky.addClass('fixed');
	 sticky.show();
	 } else{ 
	 sticky.removeClass('fixed');
	 sticky.hide();	
	 } 
	});
});
        jQuery(".menu-item-1705 ul li").click(function(){
            jQuery('.fusion-flyout-menu-toggle').trigger("click");
        });
        jQuery(".menu-item-1122 ul li").click(function(){
            jQuery('.fusion-flyout-menu-toggle').trigger("click");
        });
</script>
<?php else:?>
<script type="text/javascript">
jQuery(document).ready(function(){	
	jQuery(window).scroll(function(){
  	var sticky = jQuery('.method-sticky-header'),
    scroll = jQuery(window).scrollTop();
	 if (scroll >= 100) { 	
	 sticky.addClass('fixed');
	 sticky.show();
	 } else{ 
	 sticky.removeClass('fixed');
	 sticky.hide();	
	 } 
	});
});
</script>
<script type="text/javascript">
jQuery(document).ready(function () {
setTimeout(function(){ 
var url = jQuery(location).attr('href');
var id = url.substring(url.lastIndexOf('#') + 1);
id = decodeURI(id);
	if(id !=='certificates'){
		jQuery('html, body').animate({
				scrollTop: jQuery('#'+id+'').offset().top
			}, 'slow');
	}
 }, 1000);

});
</script>	
<?php endif;?>
<script>
    jQuery(".cross1").click(function(){
        jQuery(".overl-txt").hide();
        jQuery(".cross1").hide();
        jQuery(".dscr-cls").hide();

    });
</script>
<?php if(is_page('Portfolio') || is_page('Portfolj') || is_page('Portfölj')): ?>
<script type="text/javascript">

	//Portfolio Page JS
jQuery(document).ready(function(){	
	jQuery(window).scroll(function(){
  	var sticky = jQuery('.method-sticky-header'),
    scroll = jQuery(window).scrollTop();
    console.log(scroll);
	// var section = jQuery('.fusion-fullwidth').length;
	var section = jQuery('.Sticky-custom-menu .menu-item-1710 ul li').length;
	var scrollHeight = jQuery(document).height();
	var persec_hieght = scrollHeight/section;
	var scrollPosition = jQuery(window).height() + jQuery(window).scrollTop();
	// console.log(jQuery('#page_id').val());
	//console.log(scrollHeight);
	//console.log(persec_hieght);
	//console.log(scrollPosition);
	/* Start For Portfolio page */
	/*Start in swidesh*/
	if(scroll <= 1300) {
		var $sbmenu1 = jQuery('.Sticky-custom-menu .menu-item-1710 ul li:nth-child(1) a');
		jQuery('.custom-title h1').html($sbmenu1.html());
		console.log($sbmenu1.html());

	} else if(scroll <= (1150+1300)) {
		var $sbmenu2 = jQuery('.Sticky-custom-menu .menu-item-1710 ul li:nth-child(2) a');
		jQuery('.custom-title h1').html($sbmenu2.html());

	} else if(scroll <= (750+1150+1300)) {
		var $sbmenu3 = jQuery('.Sticky-custom-menu .menu-item-1710 ul li:nth-child(3) a');	
		jQuery('.custom-title h1').html($sbmenu3.html());

	} else if(scroll <= (600+750+1150+1300)) {
		var $sbmenu4 = jQuery('.Sticky-custom-menu .menu-item-1710 ul li:nth-child(4) a');
		jQuery('.custom-title h1').html($sbmenu4.html());

	} else {
		var $sbmenu5 = jQuery('.Sticky-custom-menu .menu-item-1710 ul li:nth-child(5) a');
		jQuery('.custom-title h1').html($sbmenu5.html());
	}	
	/*start For english */
	if(scroll <= 1300) {
		var $sbmenu1 = jQuery('.Sticky-custom-menu .menu-item-1123 ul li:nth-child(1) a');
		jQuery('.custom-title h1').html($sbmenu1.html());
		console.log($sbmenu1.html());

	} else if(scroll <= (1150+1300)) {
		var $sbmenu2 = jQuery('.Sticky-custom-menu .menu-item-1123 ul li:nth-child(2) a');
		jQuery('.custom-title h1').html($sbmenu2.html());

	} else if(scroll <= (750+1150+1300)) {
		var $sbmenu3 = jQuery('.Sticky-custom-menu .menu-item-1123 ul li:nth-child(3) a');	
		jQuery('.custom-title h1').html($sbmenu3.html());

	} else if(scroll <= (600+750+1150+1300)) {
		var $sbmenu4 = jQuery('.Sticky-custom-menu .menu-item-1123 ul li:nth-child(4) a');
		jQuery('.custom-title h1').html($sbmenu4.html());

	} else {
		var $sbmenu5 = jQuery('.Sticky-custom-menu .menu-item-1123 ul li:nth-child(5) a');
		jQuery('.custom-title h1').html($sbmenu5.html());
	}
	/*End For english */
	/* Start For Method page */
	 if (scroll >= 100) { 	
	 sticky.addClass('fixed');
	 sticky.show();
	 } else{ 
	 sticky.removeClass('fixed');
	 sticky.hide();	
	 } 
	});
});
        jQuery(".menu-item-1710 ul li").click(function(){
            jQuery('.fusion-flyout-menu-toggle').trigger("click");
        });
        jQuery(".menu-item-1123 ul li").click(function(){
            jQuery('.fusion-flyout-menu-toggle').trigger("click");
        });
</script>

<?php else:?>
<script type="text/javascript">
jQuery(document).ready(function(){	
	jQuery(window).scroll(function(){
  	var sticky = jQuery('.method-sticky-header'),
    scroll = jQuery(window).scrollTop();
	 if (scroll >= 100) { 	
	 sticky.addClass('fixed');
	 sticky.show();
	 } else{ 
	 sticky.removeClass('fixed');
	 sticky.hide();	
	 } 
	});
});
</script>
<script type="text/javascript">
jQuery(document).ready(function () {
	//setTimeout(function(){ 
		var url = jQuery(location).attr('href');
		var id = url.substring(url.lastIndexOf('#') + 1);
		id = decodeURI(id);

		if(id==='certificates') {
			console.log('cet');
			jQuery('html, body').animate({
				'scrollTop' : jQuery("#certificates").position().top-100
			});

		}else{
			jQuery('html, body').animate({
					scrollTop: jQuery('#'+id+'').offset().top
			}, 'slow');
		
		}
	//}, 3000);

});
</script>	
<?php endif;?>

<?php if(is_page('neox') || is_page('neox') ): ?>
<script type="text/javascript">
//NEOX page JS
jQuery(document).ready(function(){	
	jQuery(window).scroll(function(){
  	var sticky = jQuery('.method-sticky-header'),
    scroll = jQuery(window).scrollTop();
    console.log(scroll);
	// var section = jQuery('.fusion-fullwidth').length;
	var section = jQuery('.Sticky-custom-menu .menu-item-1127 ul li').length;
	var scrollHeight = jQuery(document).height();
	var persec_hieght = scrollHeight/section;
	var scrollPosition = jQuery(window).height() + jQuery(window).scrollTop();
	/* Start For Portfolio page */
	/*Start in swidesh*/
	if((scroll >= 200) && (scroll <= 1200)) {
		var $sbmenu1 = jQuery('.Sticky-custom-menu .menu-item-1718 ul li:nth-child(1) a');
		jQuery('.custom-title h1').html($sbmenu1.html());
		console.log($sbmenu1.html());

	} else if((scroll >= 1200) && (scroll <= (200+1200))) {
		var $sbmenu2 = jQuery('.Sticky-custom-menu .menu-item-1718 ul li:nth-child(2) a');
		jQuery('.custom-title h1').html($sbmenu2.html());

	} else {
		var $sbmenu3 = jQuery('.Sticky-custom-menu .menu-item-1718 ul li:nth-child(3) a');	
		jQuery('.custom-title h1').html($sbmenu3.html());

	}	
	/*start For english */

	if((scroll >= 200) && (scroll <= 1200)) {
		var $sbmenu1 = jQuery('.Sticky-custom-menu .menu-item-1127 ul li:nth-child(1) a');
		jQuery('.custom-title h1').html($sbmenu1.html());
		console.log($sbmenu1.html());

	} else if((scroll >= 1353) && (scroll <= (1653))) {
		var $sbmenu2 = jQuery('.Sticky-custom-menu .menu-item-1127 ul li:nth-child(2) a');
		jQuery('.custom-title h1').html($sbmenu2.html());

	} else if(scroll >= (1653)) {
		var $sbmenu3 = jQuery('.Sticky-custom-menu .menu-item-1127 ul li:nth-child(3) a');	
		jQuery('.custom-title h1').html($sbmenu3.html());

	} else {
        }
	/*End For english */
	/* Start For NEOX page */
	 if (scroll >= 100) { 	
	 sticky.addClass('fixed');
	 sticky.show();
	 } else{ 
	 sticky.removeClass('fixed');
	 sticky.hide();	
	 } 
	});
});
var lng = jQuery("html").attr('lang');
if('sv-SE' == lng){
	//SE NEOX page js Start
		var $OmCapital = jQuery('.menu-item-1718 ul li:nth-child(1) a');
		$OmCapital.on('click',function() {
			console.log(jQuery("#Om-Neox-Capital").position());
			jQuery('.fusion-flyout-menu-toggle').trigger("click");
		    jQuery('html, body').animate({
		        'scrollTop' : jQuery("#Om-Neox-Capital").position().top
		    });	
	    });	
	    var $Jobba_oss = jQuery('.menu-item-1718 ul li:nth-child(2) a');
		$Jobba_oss.on('click',function() {
			console.log(jQuery("#Jobba-hos-oss").position());
			jQuery('.fusion-flyout-menu-toggle').trigger("click");
		    jQuery('html, body').animate({
		        'scrollTop' : jQuery("#Jobba-hos-oss").position().top-300
		    });
		});     
		var $Legalt = jQuery('.menu-item-1718 ul li:nth-child(3) a');
		$Legalt.on('click',function() {
			console.log(jQuery("#Legalt").position());
			jQuery('.fusion-flyout-menu-toggle').trigger("click");
		    jQuery('html, body').animate({
		        'scrollTop' : jQuery("#Legalt").position().top-150
		    });
		}); 
//SE NEOX page js End	
//Visit NEOX SE page js hit after load start
jQuery(document).ready(function () {
	setTimeout(function(){ 
		var url = jQuery(location).attr('href');
		var id = url.substring(url.lastIndexOf('#') + 1);
		id = decodeURI(id);
		if(id==='Om-Neox-Capital') {
			console.log(id);
			jQuery('html, body').animate({
		        'scrollTop' : jQuery("#Om-Neox-Capital").position().top
		    });

		} else if(id==='Jobba-hos-oss') {
			console.log(id);
			jQuery('html, body').animate({
	        	'scrollTop' : jQuery("#Jobba-hos-oss").position().top-300
	    	});

		} else if(id==='Legalt') {
			console.log(id);
			var position = jQuery("#Legalt").position().top-150;
		    jQuery('html, body').animate({
		        'scrollTop' : position
		    });	

		} else {

		}
	}, 1000);

});
//Visit NEOX Eng page js hit after load End	
} else {
	//ENG NEOX page js Start
		var $about = jQuery('.menu-item-1127 ul li:nth-child(1) a');
		$about.on('click',function() {
			console.log(jQuery("#about").position());
			jQuery('.fusion-flyout-menu-toggle').trigger("click");
		    jQuery('html, body').animate({
		        'scrollTop' : jQuery("#about").position().top
		    });	
	    });	
	    var join_team = jQuery('.menu-item-1127 ul li:nth-child(2) a');
		join_team.on('click',function() {
			console.log(jQuery("#join-our-team").position());
			jQuery('.fusion-flyout-menu-toggle').trigger("click");
		    jQuery('html, body').animate({
		        'scrollTop' : jQuery("#join-our-team").position().top-300
		    });
		});     
		var legal = jQuery('.menu-item-1127 ul li:nth-child(3) a');
		legal.on('click',function() {
			console.log(jQuery("#legal").position());
			jQuery('.fusion-flyout-menu-toggle').trigger("click");
		    jQuery('html, body').animate({
		        'scrollTop' : jQuery("#legal").position().top-150
		    });
		}); 
	//ENG NEOX page js End	
//Visit NEOX Eng page js hit after load start
jQuery(document).ready(function () {console.log('fgf');
	setTimeout(function(){ 
		var url = jQuery(location).attr('href');
		var id = url.substring(url.lastIndexOf('#') + 1);
		id = decodeURI(id);
		
		console.log(id);
		//NEOX Eng page start
		if(id==='about') {
			
			jQuery('html, body').animate({
		        'scrollTop' : jQuery("#about").position().top
		    });

		} else if(id==='join-our-team') {
			console.log(id);
			jQuery('html, body').animate({
	        	'scrollTop' : jQuery("#join-our-team").position().top-300
	    	});

		} else if(id==='legal') {
			console.log(id);
		    jQuery('html, body').animate({
		        'scrollTop' : jQuery("#legal").position().top-150
		    });	

		} else {
		
					jQuery('html, body').animate({
						'scrollTop' : jQuery("#certificates").position().top-150
					});	
		}
		//NEOX Eng page end
	}, 1000);

});
//Visit NEOX Eng page js hit after load End
}  

</script>


<?php endif;?>
<!--script type="text/javascript">
jQuery(window).scroll(function() {    
    var scroll = jQuery(window).scrollTop();
    if (scroll >=100) {
        jQuery(".avada-page-titlebar-wrapper-header").addClass("w-header");
    } else {
        jQuery(".avada-page-titlebar-wrapper-header").removeClass("w-header");
    }
});

</script-->

 



<script type="text/javascript">
jQuery(".fusion-flyout-menu-toggle").click(function(){
  jQuery(".fusion-row.method-sticky-header").toggleClass("click-menu");
});

jQuery(document).ready(function(){
	jQuery(".fusion-post-content-container p").append(" . . .");
});
 /* jQuery for Read More button in services page */
 
 
 
 
 jQuery(document).ready(function(){
	var window_width = jQuery(window).width();
	if (window_width <= 767){	
		jQuery(".fusion-meta-info").click(function(){
			jQuery(".service-artical").appendTo(this);
		});
	}	
 });

 
 jQuery(".arrow-sign .category-services").on('click','.fusion-read-more',function(e) {
	 jQuery(this).parent().append("<div class='spinner-border loader_readmore' role='status'><span class='sr-only'>Loading...</span></div>");
	  var href = jQuery(this).attr('href');
	  var thisDom=jQuery(this);
	  e.preventDefault();
	  var articalid =  jQuery(this).parents("article").attr('id');
	  //console.log(articalid );
	  jQuery('.fusion-posts-container').find("article").removeClass('active_class');
	  jQuery('#'+articalid).addClass('active_class');
	  var values={'articalid':articalid, 'action':'service_content'};
	  jQuery(document).find('.fusion-read-less').text('Read More').addClass('fusion-read-more').removeClass('fusion-read-less');
      jQuery.ajax({
         type : "post",
         dataType : "json",
         url : '<?php echo admin_url('admin-ajax.php');?>',
         data :values,
         success: function(response) {
			 //jQuery(".loader_readmore").show();
			thisDom.text('Read Less').removeClass('fusion-read-more').addClass('fusion-read-less');
            if(response.status == "success") {
			   jQuery(".loader_readmore").hide();
			   jQuery(".service-artical").html(response.content);
			   jQuery(".service-artical").show();
            }
         }
      }) 
});
	jQuery(".arrow-sign .category-services").on('click','.fusion-read-less',function(e) {
		e.preventDefault();
		jQuery(this).text('Read More').addClass('fusion-read-more').removeClass('fusion-read-less');
		jQuery(".service-artical").hide();
		
});

 /* remove active class  */
jQuery(".arrow-sign .category-services").on('click','.fusion-read-less',function(e) {
	  var href = jQuery(this).attr('href');
	  var thisDom=jQuery(this);
	  e.preventDefault();
	  var articalid =  jQuery(this).parents("article").attr('id');
	  console.log(articalid );
	  jQuery('.fusion-posts-container').find("article").removeClass('active_class');
	  jQuery('#'+articalid).removeClass('active_class');

});

jQuery(document).on('shown.bs.tab', 'a[data-toggle="tab"]', function (e) {
    var tab = jQuery(e.target);
    var contentId = tab.attr("href");
    if (tab.parent().hasClass('active')) {
		 var current=jQuery(contentId).find('.current_isin').text();
		 var current1=jQuery(contentId).find('.current_down').text();
		 var current2=jQuery(contentId).find('.current_sheet a').attr('href');
		 var currentkiid=jQuery(contentId).find('.current_kiid a').attr('href');
		 var currentText=jQuery(contentId).find('.current_sheet a').text();
		 var currentTextkiid=jQuery(contentId).find('.current_kiid a').text();
		// console.log('the tab with the content id ' + contentId + ' is NOT visible');
		 jQuery('.isin').text(current);
		 jQuery('.down').text(current1);
		 jQuery(".sheet").attr("href",current2);
		 jQuery(".kiid").attr("href",currentkiid);
		 jQuery('.sheet').text(currentText);
		 jQuery('.kiid').text(currentTextkiid);
		//console.log(current);
		console.log(currentText);
		console.log(current2);
    } else {
         console.log('the tab with the content id ' + contentId + ' is NOT visible');
    }
});

		/* this jQuery for read more button in another page */
		
	jQuery(".read_more_custom div a.fusion-read-more").click( function(e) {
	  var redirect_url = jQuery(this).attr('href');
	  e.preventDefault();
		window.location.href=redirect_url;
	});
</script>

<script>
	/* jQuery(document).ready(function(){
		jQuery(".fusion-read-more").click(function(){
			jQuery(".service-artical").toggleClass("active");
		});
	});
	
 /*class add for raed more button in services page*/

	/* jQuery(document).ready(function(){
	jQuery(".read_more").click(function(){
  jQuery(".fusion-read-more").toggleClass("service_layout");
});
}); */
	 
	
/* 	 jQuery(document).ready(function () {
     jQuery(".fusion-alignleft").on("click", function () {
        var txt = jQuery(".fusion-read-more").is(':visible') ? 'Read More' : 'Read Less';
        jQuery(".fusion-alignleft").text(txt);
        jQuery(this).next('.fusion-read-more').slideToggle(200);
    });
});  */
</script>
<script>
</script>
	</body>
</html>