<?php
	function theme_enqueue_styles(){
		wp_enqueue_style( 'child-style', get_stylesheet_directory_uri() . '/style.css', array( 'avada-stylesheet' ) );
		wp_enqueue_style( 'wpml-legacy-horizontal-list-0', 'https://www.neoxcapital.com/wp-content/plugins/sitepress-multilingual-cms/templates/language-switchers/legacy-list-horizontal/style.css?ver=1' );
	}
	add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );

	function avada_lang_setup() {
		$lang = get_stylesheet_directory() . '/languages';
		load_child_theme_textdomain( 'Avada', $lang );
	}
	add_action( 'after_setup_theme', 'avada_lang_setup' );
		add_filter( 'retrieve_password_message', 'my_retrieve_password_message', 10, 4 );
	function my_retrieve_password_message( $message, $key, $user_login, $user_data ) {

		// Start with the default content.
		$site_name = wp_specialchars_decode( get_option( 'blogname' ), ENT_QUOTES );
		$message = __( 'You has requested a password reset for the following account:' ) . "\r\n\r\n";
		/* translators: %s: site name */
		$message .= sprintf( __( 'Site Name: %s' ), $site_name ) . "\r\n\r\n";
		/* translators: %s: user login */
		$message .= sprintf( __( 'Username: %s' ), $user_login ) . "\r\n\r\n";
		$message .= __( 'If this was a mistake, just ignore this email and nothing will happen.' ) . "\r\n\r\n";
		$message .= __( 'To reset your password, visit the following address:' ) . "\r\n\r\n";
		$message .= '<' . network_site_url( "wp-login.php?action=rp&key=$key&login=" . rawurlencode( $user_login ), 'login' ) . ">\r\n";

	   
		return $message;

	}
	/* redirect to last page visited after login */
	
	function ts_redirect_login( $redirect, $user ) {      
	return wc_get_page_permalink( 'shop' );} 
	add_filter( 'woocommerce_login_redirect', 'ts_redirect_login' );
	
	/* Shortcode for Landing page Graph*/
	function wpb_demo_shortcode(){ 
		$dataPoints = array();
		$filename=get_option( 'csv_filename' );
		$file = fopen( get_stylesheet_directory_uri().'/upload/'.$filename, 'r');
		$val=0;
		$maxp_A=0;
		$maxp_B=0;
		while(($row = fgetcsv($file)) !== FALSE){
			foreach($row as $row){
				$rowdata = explode(';',$row);
				$date= $rowdata[0];
				$maxdate=strtotime($date);
				$title= $rowdata[1];
				$currency=$rowdata[2];
				@$price=$rowdata[3];
				if($title == 'X'){
					if($maxdate > $val ){
						$maxDate = $date;
						$maxp_X = $price;
					}
					array_push($dataPoints, array("x"=>$date,"y"=> $price));
				}
				if($title == 'A'){
						$maxp_A = $price;
				}
				if($title == 'B'){
						$maxp_B = $price;
				}
			}
		}
?>
	<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
	<script type="text/javascript">
		google.charts.load('current', {'packages':['corechart']});
		google.charts.setOnLoadCallback(drawChart);
		function drawChart() {
			var data = google.visualization.arrayToDataTable([
			  ['Year',  'Neox Europe Fund'],
				<?php 
				foreach($dataPoints as $key => $result ){
					echo "['".$result['x']."',".$result['y']."],";  
				} 
				?>
			]);
			var options = {
				height: 300,
				fontSize: 15,
				legend: {position: 'top'},
				colors: ['#003366'],
			    chartArea: {
						backgroundColor: {
						gradient: {
						  color1: '#FFFFFF',
						  color2: '#99CCFF',
						  x1: '0%', y1: '40%',
						  x2: '0%', y2: '100%',
						  useObjectBoundingBoxUnits: true
						},
					  },
					  },
				title: "<?php // echo 'Last available NAV date' .'  '.' ' .$maxDate ;?>",
			   hAxis: {showTextEvery: 112},
			  vAxis: {
				format:'00',
				minValue: 0,
				ticks: [0,20,40,60,80,100,120,140,160,180,200]
			 }
			};
			var chart = new google.visualization.AreaChart(document.getElementById('chart_div'));
			chart.draw(data, options);
			var maxDate = '<?php echo $maxDate;?>';
			var maxpriceA = '<?php echo $maxp_A;?>';
			var maxpriceB = '<?php echo $maxp_B;?>';
			var maxpriceX = '<?php echo $maxp_X;?>';
				 jQuery('.class-landing').text(maxDate);
				 jQuery('.class-landing-A').text(maxpriceA);
				 jQuery('.class-landing-B').text(maxpriceB);
				 jQuery('.class-landing-X').text(maxpriceX);
		}
	</script>
<?php	
	$chart="<div id='chart_div'style='width:580px; height: 350px; overflow: hidden;'></div>";
	 return $chart;
	} 
	add_shortcode('graph', 'wpb_demo_shortcode'); 
	
	/* Shortcode for Fund page Graph by Tab A class */
	function wp_demo_shortcode(){ 
		$dataPoint = array();
		$filename=get_option( 'csv_filename' );
		$file = fopen( get_stylesheet_directory_uri().'/upload/'.$filename, 'r');
		$val=0;
		$i=1;
		while(($row = fgetcsv($file)) !== FALSE){
			foreach($row as $row){
				$rowdata = explode(';',$row);
				$date= $rowdata[0];
				$maxdate=strtotime($date);
				$title= $rowdata[1]; 
				$currency=$rowdata[2];
				@$price=$rowdata[3];
				if($title == 'A'){
					if($maxdate > $val ){
					$maxDate = $date;
					$maxp = $price;
				}
				/* if ($price > $val) {
						$val = $price;
						$maxp = $price;
					} */
				if($i ==1){
					$minDate =$rowdata[0] ;
				}
					array_push($dataPoint, array("x"=>$date,"y"=> $price));
					$i++;
				}
			}			
		}
?>
	<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
	<script type="text/javascript">
		google.charts.load('current', {'packages':['corechart']});
		google.charts.setOnLoadCallback(drawChart1);
		function drawChart1(){
			var data1 = google.visualization.arrayToDataTable([
			  ['Year',  'Neox Europe Fund class A'],
			<?php 
				foreach($dataPoint as $key => $result ){
					echo "['".$result['x']."',".$result['y']."],";  
				} 
			?>
			]);
			var options = {
				height: 300,
				fontSize: 15,
				legend: {position: 'top'},
				colors: ['#003366'],
			    chartArea: {
						backgroundColor: {
						gradient: {
						  color1: '#FFFFFF',
						  color2: '#99CCFF',
						  x1: '0%', y1: '40%',
						  x2: '0%', y2: '100%',
						  useObjectBoundingBoxUnits: true
						},
					  },
					  },
			  title: "<?php //echo $minDate.' ' . 'To' .'  '.' ' .$maxDate ;?>",
			 hAxis: {showTextEvery: 109},
			  vAxis: {
				format:'00',
				minValue: 0,
				ticks: [0,20,40,60,80,100,120,140,160,180,200]
			 },
			titleTextStyle: {
				color: ('#003366'),    // any HTML string color ('red', '#cc00cc')
				fontName: 'Roboto', // i.e. 'Times New Roman'
				fontSize: 20, // 12, 18 whatever you want (don't specify px)
				bold: false,    // true or false
				italic: false,   // true of false
			}
			
			};
			
			var chart1 = new google.visualization.AreaChart(document.getElementById('chart_div1'));
			chart1.draw(data1, options);
		}
		var maxpriceA = '<?php echo $maxp;?>';
		//console.log(maxprice);
		jQuery('.class-landing-A').text(maxpriceA);
	</script>
<?php	
	$chart1="<p class=value_A>Net asset value (NAV) per share A: " .$maxp . " EUR </p> <div id='chart_div1' style='width:650px; height:300px; overflow:hidden;'>
	</div>";
	 return $chart1;
	} 
	add_shortcode('graphA', 'wp_demo_shortcode'); 
	//add_action('wp_footer', 'wp_demo_shortcode');
	
	/* Shortcode for Fund page Graph by Tab B class */
	function wpp_demo_shortcode(){ 
	$dataPoint = array();
	$filename=get_option( 'csv_filename' );
		$file = fopen( get_stylesheet_directory_uri().'/upload/'.$filename, 'r');
		$val=0;
		$i=1;
		while(($rows = fgetcsv($file)) !== FALSE){
			foreach($rows as $rows){
				$rowdata = explode(';',$rows);
				$date= $rowdata[0];
				$maxdate=strtotime($date);
				$title= $rowdata[1]; 
				$currency=$rowdata[2];
				@$price=$rowdata[3];
				if($title == 'B'){
					if($maxdate > $val ){
						$maxDate = $date;
						$maxp = $price;
					}
				/* if ($price > $val) {
						$val = $price;
						$maxp = $val;
					} */
					if($i ==1){
						$minDate =$rowdata[0] ;
					}
					array_push($dataPoint, array("x"=>$date,"y"=> $price));
					$i++;
				}
			}			
		}
?>
	<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
		jQuery("#fusion-tab-classbsekretail,#mobile-fusion-tab-classbsekretail").click(function(){
			setTimeout(function(){
				google.charts.load('current', {'packages':['corechart']});
				google.charts.setOnLoadCallback(drawChart);
				function drawChart(){
					var data = google.visualization.arrayToDataTable([
					  ['Year', 'Neox Europe Fund class B'],
						<?php 
						foreach($dataPoint as $key => $result ){
							echo "['".$result['x']."',".$result['y']."],";  
						} 
						?>
					]);
					var options = {
						height: 300,
						fontSize: 15,
						legend: {position: 'top'},
						colors: ['#003366'],
					chartArea: {
						backgroundColor: {
						gradient: {
						  color1: '#FFFFFF',
						  color2: '#99CCFF',
						  x1: '0%', y1: '40%',
						  x2: '0%', y2: '100%',
						  useObjectBoundingBoxUnits: true
						},
					  },
					  },
					  title: "<?php //echo $minDate.' ' . 'To' .' ' .$maxDate;?>",
					hAxis: {showTextEvery: 130},
					  vAxis: {
						format:'00',
						minValue: 0,
						ticks: [0,20,40,60,80,100,120,140,160,180,200]
					 },
					titleTextStyle: {
					color: ('#003366'),    // any HTML string color ('red', '#cc00cc')
					fontName: 'Roboto', // i.e. 'Times New Roman'
					fontSize: 20, // 12, 18 whatever you want (don't specify px)
					bold: false,    // true or false
					italic: false,   // true of false
				}
					};
					var chart3 = new google.visualization.AreaChart(document.getElementById('chart_divb'));
					chart3.draw(data, options);
				}
				var maxprice = '<?php echo $maxp;?>';
				 jQuery('.class-b').text(maxprice);
				 //alert(maxprice);
			},500);
			
		});
	</script>
	<?php	
	//$chart2="<div id='chart_divb' style='width:650px; height: 300px;'></div>";
	//return $chart2;
	} 
	//add_shortcode('graphB', 'wpp_demo_shortcode');
	add_action('wp_footer', 'wpp_demo_shortcode'); 
	
	/* Shortcode for Fund page Graph by Tab X class */
	function wpi_demo_shortcode(){ 
	$dataPointt = array();
	$filename=get_option( 'csv_filename' );
		$file = fopen( get_stylesheet_directory_uri().'/upload/'.$filename, 'r');
		$val=0;
		$i=1;
		while(($rows = fgetcsv($file)) !== FALSE){
			foreach($rows as $rows){
				$rowdata = explode(';',$rows);
				$date= $rowdata[0];
				$maxdate=strtotime($date);
				$title= $rowdata[1]; 
				$currency=$rowdata[2];
				@$price=$rowdata[3];
				if($title == 'X'){
					if($maxdate > $val ){
						$maxDate = $date;
						$maxp = $price;
					}
					/* if ($price > $val) {
							$val = $price;
							$maxp = $val;
						} */
					if($i ==1){
						$minDate =$rowdata[0] ;
					}
						array_push($dataPointt, array("x"=>$date,"y"=> $price));
					$i++;
				}
			}
		}
?>
    <script type="text/javascript">
		jQuery("#fusion-tab-classxeurseed,#mobile-fusion-tab-classxeurseed").click(function(){
			setTimeout(function(){
				google.charts.load('current', {'packages':['corechart']});
				google.charts.setOnLoadCallback(drawChartI);
				function drawChartI(){
					var datai = google.visualization.arrayToDataTable([
					  ['Year', 'Neox Europe Fund class X'],
						<?php 
						foreach($dataPointt as $key => $result ){
							echo "['".$result['x']."',".$result['y']."],";  
						} 
						?>
					]);
					var options = {
						height: 300,
						fontSize: 15,
						legend: {position: 'top'},
						colors: ['#003366'],
					chartArea: {
						backgroundColor: {
						gradient: {
						  color1: '#FFFFFF',
						  color2: '#99CCFF',
						  x1: '0%', y1: '40%',
						  x2: '0%', y2: '100%',
						  useObjectBoundingBoxUnits: true
						},
					  },
					  },
					  title: "<?php //echo $minDate.' ' . 'To' .' ' .$maxDate;?>",
					hAxis: {showTextEvery: 112},
					  vAxis: {
						format:'00',
						minValue: 0,
						ticks: [0,20,40,60,80,100,120,140,160,180,200]
					 },
					titleTextStyle: {
					color: ('#003366'),    // any HTML string color ('red', '#cc00cc')
					fontName: 'Roboto', // i.e. 'Times New Roman'
					fontSize: 20, // 12, 18 whatever you want (don't specify px)
					bold: false,    // true or false
					italic: false,   // true of false
				}
					  
					};
					var chartI = new google.visualization.AreaChart(document.getElementById('chart_divI'));
					chartI.draw(datai, options);
				}
				var maxprice = '<?php echo $maxp;?>';
				 jQuery('.class-x').text(maxprice);
			},500);
		});
	</script>
	<?php	
	//$chart2="<div id='chart_divI' style='width:650px; height: 300px;'></div>";
	 //return $chart2;
	} 
	//add_shortcode('graphB', 'wpp_demo_shortcode');
	add_action('wp_footer', 'wpi_demo_shortcode'); 
	
	/* add menu for graph csv upload */
	function graph_admin_menu(){
		add_menu_page('Graph', 'Upload csv', 'manage_options', 'graph-slug', 'graph_function');
	}
	add_action('admin_menu', 'graph_admin_menu');
	function graph_function(){
?>
		<html>
			<head>
				<title>CSV file upload</title>
			</head>
			<body>
				<form enctype="multipart/form-data" action="" method="POST">
					Please choose a file: <input name="uploaded" type="file" /><br />
					<br>
					Click to upload:
					<br>
					<input type="submit" value="Upload" />
				</form> 
			</body>
		</html>
<?php
		echo get_stylesheet_directory();
		if ($_FILES["uploaded"]["error"] > 0){
		  echo "Error: " . $_FILES["uploaded"]["error"] . "<br>";
		}
		else{
			$lead_file=$_FILES["uploaded"]["name"];
			echo "Upload: " . $_FILES["uploaded"]["name"] . "<br>";
			echo "Type: " . $_FILES["uploaded"]["type"] . "<br>";
			echo "Size: " . ($_FILES["uploaded"]["size"] / 1024) . " kB<br>";
			move_uploaded_file($_FILES["uploaded"]["tmp_name"],
			get_stylesheet_directory().'/upload/' . $_FILES["uploaded"]["name"]);	 
			if(!empty($_FILES["uploaded"]["name"])){
				update_option('csv_filename',$_FILES["uploaded"]["name"]);
			}
			echo "Stored in: " . $_FILES["uploaded"]["tmp_name"];
		}
	}
/* shortcode for service content*/
	function service_content(){
	global $post;
	//$post_id = $post->ID;
	//print_r($post_id);
	$my_postid = $_POST['articalid'];//This is page id or post id
	$my_postid = trim($_POST['articalid'],"blog-1-post-");
;
	$content_post = get_post($my_postid);
	//$title = '<h1>'.get_the_title($my_postid).'</h1>';
	$content = $title.$content_post->post_content;
	$content = apply_filters('the_content', $content);
	$content = str_replace(']]>', ']]&gt;', $content);
	//echo $content;
	$response['status'] = "success";
    $response['content'] = $content;
	wp_send_json($response);
	wp_die();
	//$category_detail=get_the_category( 32 ); echo $category_detail;
	}
	add_action("wp_ajax_service_content", "service_content");
	add_action("wp_ajax_nopriv_service_content", "service_content");
	//add_action('service_content','wp_service_shortcode');
	
	
	
	
	
