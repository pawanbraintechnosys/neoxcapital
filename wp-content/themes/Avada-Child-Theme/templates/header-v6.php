<?php
/**
 * Header-v6 template.
 *
 * @author     ThemeFusion
 * @copyright  (c) Copyright by ThemeFusion
 * @link       https://theme-fusion.com
 * @package    Avada
 * @subpackage Core
 */

// Do not allow directly accessing this file.
if ( ! defined( 'ABSPATH' ) ) {
	exit( 'Direct script access denied.' );
}
?>
<?php
$object_id      = get_queried_object_id();
$c_page_id      = Avada()->fusion_library->get_page_id();
?>
<style type="text/css">
.js .tmce-active .wp-editor-area {
     color: #070707 !important; 
}
</style>
<div class="fusion-header-sticky-height"></div>
<div class="fusion-header hight-header">
	<div class="fusion-row">
		<div class="fusion-header-v6-content fusion-header-has-flyout-menu-content">
		   
		    <div class="col-sm-9 desktop-logo">
			<?php
			avada_logo();
			$menu = avada_main_menu( true ); // phpcs:ignore WordPress.WP.GlobalVariablesOverride
			?>
			</div>
			 <div class="fusion-flyout-menu-icons col-sm-1">
				<?php echo avada_flyout_menu_woo_cart(); // phpcs:ignore WordPress.Security.EscapeOutput ?>

				<?php if ( 'menu' === Avada()->settings->get( 'slidingbar_toggle_style' ) && Avada()->settings->get( 'slidingbar_widgets' ) ) : ?>
					<?php $sliding_bar_label = esc_attr__( 'Toggle Sliding Bar', 'Avada' ); ?>
					<div class="fusion-flyout-sliding-bar-toggle">
						<a href="#" class="fusion-toggle-icon fusion-icon fusion-icon-sliding-bar" aria-label="<?php echo esc_attr( $sliding_bar_label ); ?>"></a>
					</div>
				<?php endif; ?>
				<a class="fusion-flyout-menu-toggle" aria-hidden="true" aria-label="<?php esc_attr_e( 'Toggle Menu', 'Avada' ); ?>" href="#">
					<div class="fusion-toggle-icon-line"></div>
					<div class="fusion-toggle-icon-line"></div>
					<div class="fusion-toggle-icon-line"></div>
				</a>
			</div>
			<div class="col-sm-1">
			<?php dynamic_sidebar( 'Header Languages' ); ?>
			</div>
			<div class="fusion-flyout-menu-icons col-sm-1">
			<?php if ( Avada()->settings->get( 'main_nav_search_icon' ) || Avada()->settings->get( 'mobile_menu_search' ) ) : ?>
					<div class="fusion-flyout-search-toggle">
						<div class="fusion-toggle-icon">
							<div class="fusion-toggle-icon-line"></div>
							<div class="fusion-toggle-icon-line"></div>
							<div class="fusion-toggle-icon-line"></div>
						</div>
						<a class="fusion-icon fusion-icon-search" aria-hidden="true" aria-label="<?php esc_attr_e( 'Toggle Search', 'Avada' ); ?>" href="#"></a>
					</div>
			<?php endif; ?>
			</div>
      </div>
		<div class="fusion-main-menu fusion-flyout-menu" role="navigation" aria-label="Main Menu">
			<?php wp_nav_menu( array('theme_location' => 'main_navigation')); ?>
		</div>

		<?php if ( Avada()->settings->get( 'main_nav_search_icon' ) || Avada()->settings->get( 'mobile_menu_search' ) ) : ?>
			<div class="fusion-flyout-search">
				<?php get_search_form(); ?>
			</div>
		<?php endif; ?>

		<div class="fusion-flyout-menu-bg"></div>
	</div>
	<?php //if(is_page('Method') || is_page('Portfolio') || is_page('Portfölj') || is_page('Portfolj') || is_page('Metod')): ?>
	<!-- Sticky Header -->
<style type="text/css">
	.fixed {
    position: fixed;
    top:0; left:150px;
    width: 100%; }
    .custom-title {
    	max-height: 200px;
    }   
     .custom-title .fusion-page-title-row {
    	max-height: 60%;
    }     
    .menu-item-has-children ul {
    	display: none;
    }
    .Sticky-header-padd {
    	padding-left: 0px !important;
    	padding-right: 0px !important;
    }
    .fusion-header .fusion-logo {
    	margin: 10px 0px 0px 0px !important;
	}
	.funds-button .fusion-builder-row .fusion-layout-column a.fusion-button.button-flat.fusion-button-default-size.button-default.button-1.fusion-button-default-span.fusion-button-default-type {
	    font-weight: 500 ;
	    color: #000000;
	    font-size: 21px;
	    border: 1px solid #000;
	    padding: 19px 29px !important;
	    display: block;
	    margin-bottom: 15px;
	    text-align: center;
	    box-shadow: 0 .5rem 1rem rgba(0,0,0,.15)!important;
	}	
	.funds-button .fusion-builder-row .fusion-layout-column a.fusion-button.button-flat.fusion-button-default-size.button-default.button-2.fusion-button-default-span.fusion-button-default-type {
	    font-weight: 500 ;
	    color: #000000;
	    font-size: 21px;
	    border: 1px solid #000;
	    padding: 19px 29px !important;
	    display: block;
	    margin-bottom: 15px;
	    text-align: center;
	    box-shadow: 0 .5rem 1rem rgba(0,0,0,.15)!important;
	}	
	.funds-button .fusion-builder-row .fusion-layout-column a.fusion-button.button-flat.fusion-button-default-size.button-default.button-3.fusion-button-default-span.fusion-button-default-type {
	    font-weight: 500 ;
	    color: #000000;
	    font-size: 21px;
	    border: 1px solid #000;
	    padding: 19px 29px !important;
	    display: block;
	    margin-bottom: 15px;
	    text-align: center;
	    box-shadow: 0 .5rem 1rem rgba(0,0,0,.15)!important;
	}	
	.funds-button .fusion-builder-row .fusion-layout-column a.fusion-button.button-flat.fusion-button-default-size.button-default.button-4.fusion-button-default-span.fusion-button-default-type {
	    font-weight: 500 ;
	    color: #000000;
	    font-size: 21px;
	    border: 1px solid #000;
	    padding: 19px 29px !important;
	    display: block;
	    margin-bottom: 15px;
	    text-align: center;
	    box-shadow: 0 .5rem 1rem rgba(0,0,0,.15)!important;
	}	
	.funds-button .fusion-builder-row .fusion-layout-column a.fusion-button.button-flat.fusion-button-default-size.button-default.button-5.fusion-button-default-span.fusion-button-default-type {
	    font-weight: 500 ;
	    color: #000000;
	    font-size: 21px;
	    border: 1px solid #000;
	    padding: 19px 29px !important;
	    /*display: block;*/
	    margin-bottom: 15px;
	    text-align: center;
	    box-shadow: 0 .5rem 1rem rgba(0,0,0,.15)!important;
	}	
	#Avanza-button-two{
		display: none !important;
	}
	/*html {
  		scroll-behavior: smooth;
	}*/
.contact-us-wrapp textarea {
    text-transform: inherit !important;
}	
header .fusion-main-menu .menu-main-menu-container ul > li.menu-item-1125 {
    /*display: block !important;*/
}
.homeblog img.attachment-recent-posts.size-recent-posts {
     height: 250px !important; 
    /*object-fit: cover;*/
}
</style>	
		<div class="fusion-row method-sticky-header" style="display:none;">
		    <div class="fusion-header-v6-content fusion-header-has-flyout-menu-content">


			    <div class="col-sm-12 Sticky-header-padd">
				 <div class="fusion-main-menu Sticky-custom-menu" style="float:none;" role="navigation" aria-label="Main Menu">
					 <div class="col-sm-2">
						<?php
						avada_logo();
						//$menu = avada_main_menu( true ); // phpcs:ignore WordPress.WP.GlobalVariablesOverride
						?>
					</div>
					<div class="col-sm-10">
					 	<?php wp_nav_menu( array('theme_location' => 'main_navigation')); ?>
					</div>
					<div class="col-sm-12">
					  <!-- Banner -->
					  <input type="hidden" name="page_id" id="page_id" value="<?php echo $c_page_id; ?>">
							<div class="avada-page-titlebar-wrapper custom-title">
								<?php avada_current_page_title_bar( $c_page_id ); ?>
							</div>
					  <!-- Banner -->
					</div>
				</div>
			  </div>
		    </div>
		</div>
	<!-- Sticky Header -->
	<?php //endif;?>
</div>